#!/bin/env sh
# ~/.bashrc
#
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# enable tab-completion on sudo-commands
complete -cf sudo
# disable highlight on paste
bind 'set enable-bracketed-paste off'

###---------- EXPORTS ----------###
export PATH="$HOME/.local/scripts:$HOME/.local/bin:$HOME/scripts:$PATH"

export EDITOR=nvim
export TERMINAL=alacritty
export TERM=alacritty
export BROWSER="brave"
export OPENER="xdg-open"
export C__LAUNCHER="rofi"

export CMSELECTIONS="clipboard"
export CM_DEBUG=1
export CM_OUTPUT_CLIP=0
export CM_MAX_CLIPS=15
export CM_LAUNCHER="rofi"

###-------- --SHELL PROMPT ----------###

#PS1='[\u@\h \W]\$ '
#PS1="\e[00;36m\]┌─[ \e[00;37m\]\T \d \e[00;36m\]]──\e[00;31m\]>\e[00;37m\] \u\e[00;31m\]@\e[00;37m\]\h\n\e[00;36m\]|\n\e[00;36m\]└────\e[00;31m\]> \e[00;37m\]\w \e[00;31m\]\$ \e[01;37m\]"
#PS1="{\[\e[01;36m\u@\h: \]\[\e[01;34m\]\w \[\e[01;36m\]} \[\e[01;36m\]\[\$ \]\[\e[01;37m\]"
#PS1="{\[\e[01;36m\u@\h: \]\[\e[01;34m\w\]\e[m } \[\$ \]\[\e[01;37m\]"
#PS1="\[\e[1;36m\]\$(parse_git_branch)\[\033[31m\]\$(parse_git_dirty)\[\033[00m\]\n\w\[\e[1;31m\] \[\e[1;36m\]\[\e[1;37m\] "
#PS1="\[\e[1;36m\]\$(parse_git_branch)\[\033[31m\]\$(parse_git_dirty)\n\[\033[1;33m\]  \[\e[1;37m\] \w \[\e[1;36m\]\[\e[1;37m\] "


###---------- ALIAS ----------###

alias \
    ls='ls --color=auto' \
    grep='grep --color=auto' \
    diff='diff --color=auto' \
    ..='cd ..' \
    ...='cd ../../../' \
    la='ls -lhAF' \
    ll='ls -lhF' \
    rm='rm -vI'\
    rmr='rm -r' \
    rf='rm -f' \
    mv='mv -iv' \
    cp='cp -riv' \
    mkd='mkdir -pv' \
    df='df -h' \
    free='free -m' \
    np='nano -w PKGBUILD' \
    more=less \
    wget='wget -c' \
    pacman='sudo pacman' \
    vi='vim' \
    c='clear' \

#sync
alias \
    merge='xrdb -merge ~/XTerm' \
    s='source ~/.bashrc' \
        
        
#file access
alias \
    zconf='vim ~/.config/zsh/.zshrc' \
    bconf='vim ~/.config/bash/.bashrc' \
    geheim='vim ~/.geheim/vorbeiwort' \
    home='cd ~' \
    qtest='python /home/ninah/.config/qtile/config.py' \

# git (most set as git config --global)
alias dotconf='/usr/bin/git --git-dir=$HOME/repos/dotfiles/ --work-tree=$HOME'

###---------- ARCHIVE EXTRACT ----------###

ex ()
{
    if [ -f $1 ] ; then
      case $1 in
        *.tar.bz2)   tar xjf $1   ;;
        *.tar.gz)    tar xzf $1   ;;
        *.bz2)       bunzip2 $1   ;;
        *.rar)       unrar x $1   ;;
        *.gz)        gunzip $1    ;;
        *.tar)       tar xf $1    ;;
        *.tbz2)      tar xjf $1   ;;
        *.tgz)       tar xzf $1   ;;
        *.zip)       unzip $1     ;;
        *.Z)         uncompress $1;;
        *.7z)        7za e x $1   ;;
        *.deb)       ar x $1      ;;
        *.tar.xz)    tar xf $1    ;;
        *.tar.zst)   unzstd $1    ;;
        *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

take () {
    mkdir -p $1
    cd $1
}

note() {
    echo "date: $(date)" >> $HOME/documents/notes.txt
    echo "$@" >> $HOME/documents/notes.txt
    echo "" >> $HOME/documents/notes.txt
}

# Starship Prompt
eval "$(starship init bash)"
. "$HOME/.cargo/env"
