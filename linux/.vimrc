"***********************************************************
"    .vimrc
"***********************************************************
" PLUGINS
"***********************************************************

"" Call
"if filereadable(expand("~/.vimrc.plug"))
"	source ~/.vimrc.plug
"endif

call plug#begin('~/.config/.vim/plugged')

Plug 'vimwiki/vimwiki'
Plug 'ap/vim-css-color'
Plug 'tpope/vim-commentary'

" File system explorer for the Vim editor
Plug 'preservim/nerdtree'

" Filetype specific items
Plug 'ryanoasis/vim-devicons'

" Syntax highlighting to NERDTree
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'

Plug 'farmergreg/vim-lastplace' 
Plug 'Yggdroot/indentLine'

"Plug 'ap/vim-buftabline'
Plug 'itchyny/lightline.vim'
Plug 'mengelbrecht/lightline-bufferline'
Plug 'joshdick/onedark.vim'
"Plug 'arcticicestudio/nord-vim'
Plug 'ghifarit53/tokyonight-vim'
call plug#end()

"***********************************************************

let mapleader = " "

"set bg=dark
set termguicolors

" Set highlight searches
set hlsearch

" Search incrementally
set incsearch

" Set the default clipboard to the system clipboard
set clipboard+=unnamedplus

set tabstop=4
set softtabstop=4
set shiftwidth=4

set mouse=a

"***********************************************************
" BASIC STUFF
"***********************************************************
set number
set number relativenumber

" Set syntax on
set matchpairs+=<:>
set scrolloff=8

" Set backspace=indent,eol,start
set nocompatible
syntax enable
filetype plugin on
set encoding=utf-8

" Search down into subfolders
" Provides tab-completion for all file-related tasks
set path+=**

" Enable auto-completion ctrl-n to activate
set wildmode=longest,list,full

" Display all matching files when we tab complete
set wildmenu

" Disable automatic comment on newline
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Splits open at bottom and right
set splitbelow splitright

set hidden
set cursorline
"autocmd InsertEnter * highlight CursorLine guibg=#5c5c70 guifg=fg
"autocmd InsertLeave * highlight CursorLine guibg=#5c5c70 guifg=fg


"***********************************************************
" MAPPING AND REMAPS
"***********************************************************
"inoremap <c-b> <Esc>:Lex<cr>:vertical resize 30<cr>
"nnoremap <c-b> <Esc>:Lex<cr>:vertical resize 30<cr>

" Paste for systec clipboard with ctrk+i instead of shift insert
"map <S-Insert> <C-i>

" Buffer navigation
nnoremap <C-h> :bnext<CR>
nnoremap <C-l> :bprev<CR>


"***********************************************************
" Tweaks for browsing
"***********************************************************

let g:netrw_banner=0        " disable annoying banner
let g:netrw_browse_split=4  " open in prior window
let g:netrw_altv=1          " open splits to the right
let g:netrw_liststyle=3     " tree view
let g:netrw_list_hide=netrw_gitignore#Hide()
let g:netrw_list_hide.=',\(^\|\s\s\)\zs\.\S\+'

"***********************************************************
" NERD TREE
"***********************************************************
nnoremap <C-e> :NERDTreeFocus<CR>
nnoremap <leader>e :NERDTree<CR>
nnoremap <silent> <S-e> :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>




" Exit Vim if NERDTree is the only window remaining in the only tab.
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif

" Close the tab if NERDTree is the only window remaining in it.
autocmd BufEnter * if winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif


"***********************************************************
" LIGHTLINE
"***********************************************************

" Vim's auto indentation feature does not work properly with text copied from outside of Vim. 
" Press the <F2> key to toggle paste mode on/off.
"nnoremap <F2> :set invpaste paste?<CR>
"imap <F2> <C-O>:set invpaste paste?<CR>
"set pastetoggle=<F2>

let g:lightline = {
      \ 'colorscheme': 'onedark',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ], [ 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'tabline': {
      \   'left': [ ['buffers'] ],
      \   'right': [ ['close'] ]
      \ },
      \ 'component_expand': {
      \   'buffers': 'lightline#bufferline#buffers'
      \ },
      \ 'component_type': {
      \   'buffers': 'tabsel'
      \ }	
	  \}
"let g:tokyonight_style = 'night' " available night, storm
"let g:tokyonight_enable_italic = 1
"let g:tokyonight_transparent_background = 0

colorscheme onedark

" Staus bar
set laststatus=2


"***********************************************************


"	"Use 24-bit (true-color) mode in Vim/Neovim when outside tmux.
"	"If you're using tmux version 2.2 or later, you can remove the outermost $TMUX check and use tmux's 24-bit color support
"	"(see < http://sunaku.github.io/tmux-24bit-color.html#usage > for more information.)
"	if (empty($TMUX))
"	  if (has("nvim"))
"	    "For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
"	    let $NVIM_TUI_ENABLE_TRUE_COLOR=1
"	  endif
"	  "For Neovim > 0.1.5 and Vim > patch 7.4.1799 < https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
"	  "Based on Vim patch 7.4.1770 (`guicolors` option) < https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
"	  " < https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
"	  if (has("termguicolors"))
"	    set termguicolors
"	  endif
"	endif



