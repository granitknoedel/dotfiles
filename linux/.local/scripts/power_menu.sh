#!/usr/bin/env bash
yad 
--center --borders=1 --on-top --sticky --single-click --title='Logout Options' \ 
--buttons-layout=center \ 
#--window-icon="gtk-quit: exit" \ 
--button="Logout!system-log-out:kill -9 -1" \ 
--button="Reboot!system-reboot:systemctl reboot" \ 
--button="Shutdown!system-shutdown:systemctl poweroff" \ 
--button="Suspend!system-suspend:systemctl suspend" \ 
--button=Cancel:0 \