local opt = vim.opt -- for conciseness

-- line numbers
opt.number = true -- shows absolute line number on cursor line (when relative number is on)
opt.relativenumber = true -- show relative line numbers

-- tabs & indentation
opt.tabstop = 4 -- 4 spaces for tabs (prettier default)
opt.softtabstop = 4 -- keep tab indent setting for some things
opt.shiftwidth = 4 -- 4 spaces for indent width
opt.expandtab = true -- expand tab to spaces
-- opt.autoindent = true -- copy indent from current line when starting new one
opt.smartindent = true -- auto indent but with code styling

-- line wrapping
opt.wrap = false -- disable line wrapping

-- files
opt.swapfile = false
opt.backup = false
opt.undodir = os.getenv("HOME") .. "/.vim/undodir" -- neverending UNDOs
opt.undofile = true

-- search settings
-- opt.ignorecase = true -- ignore case when searching
opt.smartcase = true -- if you include mixed case in your search, assumes you want case-sensitive
opt.incsearch = true

-- cursor line
opt.cursorline = true -- highlight the current cursor line
opt.scrolloff = 8 -- keep 8 lines padding
opt.signcolumn = "yes" -- when signs are defined for a file add a column of two characters to display them in

-- for git
opt.updatetime = 50

-- appearance
opt.termguicolors = true
opt.background = "dark" -- colorschemes that can be light or dark will be made dark
opt.signcolumn = "yes" -- show sign column so that text doesn't shift

-- opt.colorcolumn = "80" -- guide for line length

-- backspace
opt.backspace = "indent,eol,start" -- allow backspace on indent, end of line or insert mode start position

-- clipboard
opt.clipboard:append("unnamedplus") -- use system clipboard as default register

-- split windows
opt.splitright = true -- split vertical window to the right
opt.splitbelow = true -- split horizontal window to the bottom

opt.iskeyword:append("-") -- consider string-string as whole word
