-- set colorscheme to nightfly with protected call
-- in case it isn't installed
--local colorscheme = "gruvbox"

---@diagnostic disable-next-line: param-type-mismatch
local status, _ = pcall(vim.cmd, "colorscheme gruvbox")
if not status then
  print("Colorscheme not found!") -- print error if colorscheme not installed
  return
end
