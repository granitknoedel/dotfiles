-- import lualine plugin safely
local status, lualine = pcall(require, "lualine")
if not status then
  return
end

--  theme
-- local lualine_nightfly = require("lualine.themes.nightfly")
local gruvbox = require("lualine.themes.gruvbox")

-- new colors for theme
local new_colors = {
  blue = "#458588",
  green = "#98971A",
  green2 = "#689D6A",
  violet = "#B16286",
  yellow = "#D79921",
  orange = "#D65D0E",
  red = "#CC241D",
  black = "#1D2021",
  grey = "#A89984",
  white = "#FBF1C7",
}

---- change nightlfy theme colors
--lualine_nightfly.normal.a.bg = new_colors.grey
--lualine_nightfly.insert.a.bg = new_colors.green2
--lualine_nightfly.visual.a.bg = new_colors.violet
--lualine_nightfly.replace.a.bg = new_colors.orange
--lualine_nightfly.command = {
--  a = {
--    gui = "bold",
--    bg = new_colors.yellow,
--    fg = new_colors.black, -- black
--  },
--}

-- change nightlfy theme colors
gruvbox.normal.a.bg = new_colors.grey
gruvbox.insert.a.bg = new_colors.green2
gruvbox.visual.a.bg = new_colors.violet
gruvbox.replace.a.bg = new_colors.orange
gruvbox.command = {
  a = {
    gui = "bold",
    bg = new_colors.yellow,
    fg = new_colors.black, -- black
  },
}
-- configure lualine with modified theme
lualine.setup({
  options = {
    theme = gruvbox,
  },
})
