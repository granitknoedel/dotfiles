-- import nvim-treesitter plugin safely
local status, treesitter = pcall(require, "nvim-treesitter.configs")
if not status then
  return
end

-- configure treesitter
treesitter.setup({

  -- enable syntax highlighting
  highlight = {
    enable = true,
    disable = { "" }, -- list of languages to disable treesitter
    additional_vim_regex_highlighting = false,
  },

  -- enable indentation
  indent = {
    enable = true,
    disable = { "yaml", "css" },
  },

  -- enable autotagging (w/ nvim-ts-autotag plugin)
  autotag = {
    enable = true,
  },

  -- ensure these language parsers are installed
  ensure_installed = {
    "bash",
    "c",
    "rust",
    "java",
    "lua",
    "python",
    "json",
    "javascript",
    "typescript",
    "tsx",
    "yaml",
    "html",
    "css",
    "markdown",
    "markdown_inline",
    "graphql",
    "vim",
    "dockerfile",
    "gitignore",
  },

  -- auto install above language parsers
  auto_install = true,
})
