# QTILE CONFIG FILE
# ~/.config/qtile/config.py

from libqtile.dgroups import simple_key_binder
import os
import re

# import socket
import subprocess

# import datetime
# import string
import subprocess

# import dateutil.parser
from libqtile import hook
from libqtile import qtile
from libqtile import bar, layout, widget
from libqtile.config import (
    Click,
    Drag,
    Group,
    Key,
    Match,
    Screen,
    Rule,
)  # , KeyChord, ScratchPad, DropDown
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

# from libqtile.widget import Spacer, Backlight
# from libqtile.widget.image import Image
# from typing import List
from qtile_extras import widget
from qtile_extras.widget.decorations import BorderDecoration

import colors

mod = "mod4"
altmod = "mod1"
terminal = guess_terminal()
HOME = "/home/ninah"
logouMenu = "kitty -e powermenu.sh"

# Variables
browser = "brave"
terminal1 = "alacritty"
terminal2 = "kitty"
text_editor = terminal1 + " nvim"
file_manager1 = "thunar"
file_manager2 = terminal1 + " ranger"
file_launcher1 = "rofi -show run"
file_launcher2 = "dmenu_run"
email_client = "thunderbird"
process_viewer = terminal1 + " htop"

colors = [
    ["#282c34", "#282c34"],  # 0 mid grey 21
    ["#1c1f24", "#1c1f24"],  # 1 dark grey 20
    ["#dfdfdf", "#dfdfdf"],  # 2 light grey 25
    ["#ff6c6b", "#ff6c6b"],  # 3 coral 12
    ["#98be65", "#98be65"],  # 4 lime 18
    ["#da8548", "#da8548"],  # 5 orange 19
    ["#51afef", "#51afef"],  # 6 light blue 16
    ["#c678dd", "#c678dd"],  # 7 pink 17
    ["#46d9ff", "#46d9ff"],  # 8 cyan
    ["#a9a1e1", "#a9a1e1"],  # 9 violet
    # Gruvbox Palette
    ["#282828", "#282828"],  # 10 Background (0/5)
    ["#928374", "#928374"],  # 11 Grey
    ["#CC241D", "#CC241D"],  # 12 Red
    ["#fb4934", "#fb4934"],  # 13 Red
    ["#98971A", "#98971A"],  # 14 Green
    ["#D79921", "#D79921"],  # 15 Yellow
    ["#458588", "#458588"],  # 16 Blue
    ["#B16286", "#B16286"],  # 17 Purple
    ["#689D6A", "#689D6A"],  # 18 Aqua
    ["#D65D0E", "#D65D0E"],  # 19 Orange
    ["#A89984", "#A89984"],  # 20 Foreground (0/4)
    ["#1D2021", "#1D2021"],  # 21 Background/Foreground 0 (Hard)
    ["#32302F", "#32302F"],  # 22 Background/Foreground 0 (Soft)
    ["#3C3836", "#3C3836"],  # 23 Background/Foreground 1
    ["#504945", "#504945"],  # 24 Background/Foreground 2
    ["#665C54", "#665C54"],  # 25 Background/Foreground 3
    ["#bdae93", "#bdae93"],  # 26 Background/Foreground 3
    ["#fbf1c7", "#fbf1c7"],  # 27 Background/Foreground 3
    ["#7C6F64", "#7C6F64"],
]  # 28 Background/Foreground 4

keys = [
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    # Switch between windows
    Key([mod], "left", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "right", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "down", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "up", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),
    # Move windows between left/right/up/down in current stack.
    Key(
        [mod, "shift"],
        "left",
        lazy.layout.shuffle_left(),
        desc="Move window to the left",
    ),
    Key(
        [mod, "shift"],
        "right",
        lazy.layout.shuffle_right(),
        desc="Move window to the right",
    ),
    Key([mod, "shift"], "down", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "up", lazy.layout.shuffle_up(), desc="Move window up"),
    # Grow/Shrink windows
    Key(
        [mod, "control"],
        "left",
        lazy.layout.grow_left(),
        desc="Grow window to the left",
    ),
    Key(
        [mod, "control"],
        "right",
        lazy.layout.grow_right(),
        desc="Grow window to the right",
    ),
    Key([mod, "control"], "down", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "up", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    # Monitors
    Key([mod], "period", lazy.next_screen(), desc="Move focus to next monitor"),
    Key([mod], "comma", lazy.prev_screen(), desc="Move focus to prev monitor"),
    Key([mod, "control"], "f", lazy.window.toggle_floating(), desc="Toggle floating"),
    Key([mod], "f", lazy.window.toggle_fullscreen(), desc="toggle fullscreen"),
    # Stack controls
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod, "shift"], "Return", lazy.layout.flip(), desc="Flip Sides in monadtall"),
    Key(  # DUPLICATE
        [mod, "shift"], "Tab", lazy.layout.flip(), desc="Flip Sides in monadtall"
    ),
    Key(  # DUPLICATE
        [mod, "shift"], "space", lazy.layout.flip(), desc="Flip Sides in monadtall"
    ),
    Key(
        [mod, "shift"],
        "j",
        lazy.layout.shuffle_down(),
        lazy.layout.section_down(),
        desc="Move windows down in current stack",
    ),
    # Mapping for arrows
    Key(
        [mod, "shift"],
        "down",
        lazy.layout.shuffle_down(),
        lazy.layout.section_down(),
        desc="Move windows down in current stack",
    ),
    Key(
        [mod, "shift"],
        "k",
        lazy.layout.shuffle_up(),
        lazy.layout.section_up(),
        desc="Move windows up in current stack",
    ),
    # Mapping for arrows
    Key(
        [mod, "shift"],
        "up",
        lazy.layout.shuffle_up(),
        lazy.layout.section_up(),
        desc="Move windows up in current stack",
    ),
    Key([mod], "j", lazy.group.next_window(), desc="[Layout] Focus next window"),
    Key([mod], "k", lazy.group.prev_window(), desc="[Layout] Focus previous window"),
    Key(
        [mod],
        "l",
        lazy.layout.shrink(),
        lazy.layout.decrease_nmaster(),
        desc="Shrink window (MonadTall), decrease number in master pane (Tile)",
    ),
    Key(
        [mod],
        "h",
        lazy.layout.grow(),
        lazy.layout.increase_nmaster(),
        desc="Expand window (MonadTall), increase number in master pane (Tile)",
    ),
    # Programs
    Key([mod], "Return", lazy.spawn(terminal1), desc="Launch terminal1"),
    Key(
        [mod, "control"],
        "v",
        lazy.spawn(
            "rofi -modi \"clipboard:greenclip print\" -show clipboard -run-command '{cmd}' ; sleep 0.5; xdotool type $(xclip -o -selection clipboard)"
        ),
        desc="Clipboard using rofi",
    ),
    Key([altmod], "r", lazy.spawn("dmenu_run"), desc="Spawn a command using dmenu"),
    Key([mod], "r", lazy.spawn("rofi -show run"), desc="Spawn a command using rofi"),
    Key(
        [mod],
        "d",
        lazy.spawn("rofi -modi fb:~/.config/rofi/scripts/rofi-filebrowser.sh -show fb"),
        desc="Spawn a command using a prompt widget",
    ),
    Key(
        [mod],
        "t",
        lazy.spawn("rofi -show calc"),
        desc="Spawn a command using a prompt widget",
    ),
    Key(
        [mod],
        "z",
        lazy.spawn(
            "rofi -modi emoji -show emoji -emoji-format '{emoji}' -i -theme-str '@import \"grid.rasi\"'"
        ),
        desc="Spawn a command using a prompt widget",
    ),
    Key(
        [mod],
        "m",
        lazy.spawn(
            "rofi -show power-menu -modi power-menu:~/.local/scripts/rofi-power-menu -i -theme-str '@import \"power.rasi\"'"
        ),
        desc="Spawn a command using a prompt widget",
    ),
    Key([mod], "e", lazy.spawn("thunar"), desc="GUI Filemanager"),
    Key([mod, "shift"], "e", lazy.spawn("alarcitty -e ranger"), desc="TUI Filemanager"),
    # Sound
    Key(
        [],
        "XF86AudioMute",
        lazy.spawn("pactl set-sink-mute 0 toggle"),
        desc="Mute sound",
    ),
    Key(
        [],
        "XF86AudioLowerVolume",
        lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ -5%"),
        desc="Volume down",
    ),
    Key(
        [],
        "XF86AudioRaiseVolume",
        lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ +5%"),
        desc="Volume up",
    ),
    Key([], "Print", lazy.spawn("flameshot gui"), desc="Screenshot Area"),
    # Key([mod], "m", lazy.group['powermenu'].dropdown_toggle('exitMenu')),
    # Key([mod, "shift"], "v", lazy.group['pavuctl'].dropdown_toggle('term')),
    # Key([mod, "shift"], "o", lazy.group['katz'].dropdown_toggle('katzi')),
    # Key([mod, "shift"], "e", lazy.group['edit'].dropdown_toggle('editConf')),
    # Key([mod, "shift"], "k", lazy.group['solaar'].dropdown_toggle('logi')),
]

groups = [
    Group(
        "1 DEV", layout="mdtall", label="1 DEV", layout_opts={"ratio": 0.55}, position=1
    ),
    Group(
        "2 WEB",
        layout="mdtaller",
        label="2 WEB",
        #            layout_opts={"ratio": 0.85},
        layout_opts={"ratio": 0.8, "master_match": Match(wm_class=["brave"])},
        spawn=["brave", "chromium"],
        position=2,
    ),
    Group(
        "3 WRK",
        layout="max",
        label="3 WRK",
        matches=[Match(wm_class=["microsoft-edge"])],
        exclusive=True,  # init=True, persist=True,
        position=3,
    ),
    Group(
        "4 MSG",
        layout="tile",
        label="4 MSG",
        matches=[Match(wm_class=["Mail", "Msgcompose", "Signal", "telegram-desktop"])],
        exclusive=True,  # init=True, persist=True,
        position=4,
    ),
    Group("5 DOC", layout="mdtall", label="5 DOC", position=5),
    Group(
        "6 CHT",
        layout="mdtall",
        label="6 CHT",
        matches=[Match(wm_class=["discord", "spotify"])],
        exclusive=True,  # init=True, persist=True,
        position=6,
    ),
    Group(
        "7 GFX",
        layout="max",
        label="7 GFX",
        matches=[Match(wm_class=["Gimp", "obs"])],
        position=7,
    ),
    Group(
        "8 VID",
        layout="treetab",
        label="8 VID",
        matches=[Match(wm_class=["zoom"])],
        position=8,
    ),
    Group(
        "9 MSC",
        layout="mdtall",
        label="9 MSC",
        # spawn=['spotify'],
        position=9,
    ),
    #          ScratchPad("scratchpad",[
    #            DropDown("zoom", "", x=0.05, y=0.02, width=0.90, height=0.6, on_focus_lost_hide=True),
    #            DropDown("tunes", "", x=0.05, y=0.02, width=0.90, height=0.6, on_focus_lost_hide=False)
    #            ]
    #          ),
    # ScratchPad("powermenu",[DropDown("exitMenu", "rofi -show power-menu -modi power-menu:~/.local/scripts/rofi-power-menu -i -theme-str \'@import \"power.rasi\"\'", x=0.4, y=0.2, width=0.20, height=0.20, on_focus_lost_hide=False)]),
    # ScratchPad("pavuctl",[DropDown("term", "xterm -e pavucontrol", x=0.4, y=0.02, width=0.40, height=0.30, on_focus_lost_hide=False)]),
    # ScratchPad("katz",[DropDown("katzi", "kitty", x=0.30, y=0.02, width=0.40, height=0.30, on_focus_lost_hide=False)]),
    # ScratchPad("music",[DropDown("tunes", "alacritty -e ncmpcpp", x=0.05, y=0.02, width=0.90, height=0.6, on_focus_lost_hide=False)]),
    # ScratchPad("solaar",[DropDown("logi", "xterm -e solaar", x=0.33, y=0.04, width=0.35, height=0.45, on_focus_lost_hide=False)]),
]

# dgroup rules that not belongs to any group
dgroups_app_rules = [
    # Everything i want to be float, but don't want to change group
    Rule(
        Match(
            title=["nested", "gscreenshot"],
            wm_class=[
                "knip",
                "Florence",
                "Plugin-container",
                "Terminal",
                "Gpaint",
                "Kolourpaint",
                "Wrapper",
                "Gcr-prompter",
                "Ghost",
                "Gnuplot",
                "Pinta",
                "nitrogen",
                "pavucontrol",
                "flameshot",
                "lxappearance",
                #'solaar',
                "nitrogen",
                "feh",
                "geeqie",
                re.compile("Gnome-keyring-prompt.*?"),
            ],
            wm_type=["dialog", "utility", "splash"],
        ),
        float=True,
        intrusive=True,
    ),
    # floating windows
    #    Rule(Match(wm_class=['pavucontrol',
    #    					 'flameshot',
    #    					 'lxappearance',
    #'solaar',
    #    					 'nitrogen',
    #    					 'feh',
    #    					 'geeqie'
    #    					 ],
    # title=[re.compile('[a-zA-Z]*? Steam'),
    #        re.compile('Steam - [a-zA-Z]*?')]
    # title=['Zoom - Licensed Account'],
    #               ),
    #         float=True
    #   ),
    # static windows (unmanaged)
    # Rule(Match(wm_class=["XTerm"]), static=True),
    # Rule(Match(net_wm_pid=["XTerm"]), float=True),
]


floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
        # Match(title="Confirmation"),
        Match(title="Write: (no subject) - Thunderbird"),
        Match(wm_class="pavucontrol"),  # Sound controls
        Match(wm_class="flameshotS"),  # Screenshots
        Match(wm_class="lxappearance"),  # Themeing
        Match(wm_class="solaar"),  # Logitech BluetoSFsoth Management
        Match(wm_class="piper"),  # Logitech Mouse Management
        Match(wm_class="nitrogen"),  # Wallpapers
        Match(wm_class="galculator"),  # Calculator
        Match(wm_class="xterm"),  # xterm for updates
        # Match(title="Zoom - Licensed Account"),  # Zoom Main Window
        # Match(title="Wie möchten Sie an der Audiokonferenz teilnehmen?"),  # Zoom Audio Window
        Match(wm_class="geeqie"),  # xterm for updates
    ]
    # border_focus:"#c678dd",
)

# Allow MODKEY+[0 through 9] to bind to groups, see https://docs.qtile.org/en/stable/manual/config/groups.html
# MOD4 + index Number : Switch to Group[index]
# MOD4 + shift + index Number : Send active window to another Group
dgroups_key_binder = simple_key_binder("mod4")

# Drag floating layouts.
mouse = [
    Drag(
        [mod],
        "Button1",
        lazy.window.set_position_floating(),
        start=lazy.window.get_position(),
    ),
    Drag(
        [mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()
    ),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

follow_mouse_focus = True
bring_front_click = True
# cursor_warp = False
focus_on_window_activation = "smart"  # smart focus or never
reconfigure_screens = True

# auto-minimize windows when losing focus
auto_minimize = False
auto_fullscreen = False

layout_theme = {
    "border_width": 3,
    "margin": 6,
    "border_focus": colors[18],
    "border_normal": colors[10],
}

layouts = [
    layout.MonadTall(name="mdtall", ratio=0.6, **layout_theme),
    layout.MonadTall(name="mdtaller", ratio=0.8, **layout_theme),
    # layout.RatioTile(ratio=3.80, **layout_theme),
    layout.Max(),
    # layout.Columns(**layout_theme),
    # layout.Stack(num_stacks=2, ratio=0.70, **layout_theme),
    # layout.Bsp(**layout_theme),
    # layout.Matrix(**layout_theme),
    # layout.MonadWide(**layout_theme),
    # layout.Tile(name="Mail Tile", ratio=0.70, expand=True, **layout_theme),
    layout.Tile(name="tile", ratio=0.70, expand=True, **layout_theme),
    layout.TreeTab(
        font="NotoSans NF Condensed SemiBold",
        fontsize=12,
        margin_y=20,
        sections=["MAIN", "SUB"],
        section_top=10,
        section_padding=10,
        section_bottom=20,
        section_fontsize=12,
        section_fg="A89984",
        section_left=5,
        border_width=0,
        bg_color="282828",
        active_bg="689D6A",
        active_fg="282828",
        inactive_bg="3C3836",
        inactive_fg="A89984",
        urgent_bg="D79921",
        urgent_fg="282828",
        padding_left=0,
        padding_x=5,
        padding_y=5,
        level_shift=0,
        vspace=8,
        panel_width=150,
        place_right=True,
    ),
    # layout.VerticalTile(**layout_theme),
    # layout.Zoomy(**layout_theme),
]

# bg-col:  #282828;
# bg-col-light: #32302f;
# border-col: #689d6a;
# selected-col: #3C3836;
# blue: #458588;
# fg-col: #fbf1c7;
# fg-col2: #A89984;
# grey: #928374;
# lime: #98971A;
# green: #689D6A;


# Parse String for Window Name Widget
def longNameParse(text):
    for string in [
        "Chromium",
        "Firefox",
        "Brave",
        "Thunderbird",
        "Telegram",
        "Eigener PC",
    ]:
        if string in text:
            text = string
        else:
            text = text
    return text


widget_defaults = dict(
    font="NotoSans NF SemiBold",
    # font="NotoMono NF SemiBold",
    # font="JetBrainsMono NF Bold",
    # font="NotoSansMono NFM Bold",
    # font="RobotoMono NFM Bold",
    # font="Roboto Bold",
    # font="TerminessTTF NFM Bold",
    # font="UbuntuMono NFM Bold",
    # font="Ubuntu NF Bold",
    # font="PowerlineSymbols Bold",
    # font="Mononoki NFM Bold",
    # font="Mononoki NF Bold",
    # font="MesloLGS NFM Bold",
    # font="MesloLGS NF Bold",
    # font="Hack NFM Bold",
    # font="Hack NF Bold",
    # font="Iosevka Nerd Font Mono Bold",
    # font="Font Awesome v4 Compatibility Bold",
    # font="FiraCode NFM Bold",
    # font="Hack NF Bold",
    fontsize=14,
    padding=15,
    background=colors[21],
)
extension_defaults = widget_defaults.copy()


def init_widgets_list():
    widgets_list = [
        # 0
        widget.Sep(
            linewidth=0, padding=6, foreground=colors[26], background=colors[10]
        ),
        # 1
        widget.Image(
            # margin = 8,
            margin_y=4,
            margin_x=10,
            filename="~/.config/qtile/icons/arch1.png",
            # filename = "~/.config/qtile/icons/batman02.png",
            scale="True",
            background=colors[0],
            mouse_callbacks={"Button1": lambda: qtile.cmd_spawn(terminal1)},
        ),
        # 2
        widget.Sep(
            linewidth=0, padding=6, foreground=colors[26], background=colors[10]
        ),
        # 3
        widget.GroupBox(
            font="Ubuntu Bold",
            fontsize=12,
            margin_y=3,
            margin_x=6,
            padding_y=2,
            padding_x=6,
            borderwidth=3,
            active=colors[26],
            inactive=colors[25],
            rounded=False,
            highlight_color=colors[25],
            highlight_method="default",
            urgent_alert_method="text",
            # urgent_border=colors[5],
            urgent_text=colors[5],
            this_current_screen_border=colors[18],
            this_screen_border=colors[18],
            other_current_screen_border=colors[26],
            other_screen_border=colors[26],
            foreground=colors[25],
            background=colors[10],
        ),
        # 4
        widget.TextBox(
            text="|",
            font="Ubuntu Mono",
            background=colors[10],
            foreground="474747",
            padding=2,
            fontsize=14,
        ),
        # 5
        widget.CurrentLayoutIcon(
            # custom_icon_paths=[os.path.expanduser("~/.config/qtile/icons")],
            foreground=colors[25],
            background=colors[10],
            padding=0,
            scale=0.5,
        ),
        # 6
        widget.CurrentLayout(foreground=colors[25], background=colors[10], padding=5),
        # 7
        widget.TextBox(
            text="|   ",
            font="Ubuntu Mono",
            background=colors[10],
            foreground="474747",
            padding=2,
            fontsize=14,
        ),
        # 8
        widget.WindowName(
            parse_text=longNameParse,
            foreground=colors[18],
            background=colors[10],
            padding=0
            # format = '{name}'
        ),
        # 9
        widget.Systray(background=colors[10], padding=5),
        # 10
        widget.Sep(
            linewidth=0, padding=6, foreground=colors[10], background=colors[10]
        ),
        widget.Net(
            interface="enp0s31f6",
            format="NET  {down}  {up}",  #   ↓↑      󰧇 󰦿   󰜷󰜮
            foreground=colors[20],
            background=colors[10],
            padding=5,
            decorations=[
                BorderDecoration(
                    colour=colors[20],
                    border_width=[0, 0, 2, 0],
                    padding_x=5,
                    padding_y=4,
                )
            ],
        ),
        widget.Sep(
            linewidth=0, padding=6, foreground=colors[10], background=colors[10]
        ),
        # 13
        widget.ThermalSensor(
            foreground=colors[20],
            background=colors[10],
            threshold=90,
            fmt="TEMP {}",
            padding=5,
            decorations=[
                BorderDecoration(
                    colour=colors[20],
                    border_width=[0, 0, 2, 0],
                    padding_x=5,
                    padding_y=4,
                )
            ],
        ),
        widget.Sep(
            linewidth=0, padding=6, foreground=colors[10], background=colors[10]
        ),
        # 15
        widget.Memory(
            foreground=colors[20],
            background=colors[10],
            mouse_callbacks={
                "Button1": lambda: qtile.cmd_spawn(terminal1 + " -e htop")
            },
            fmt="MEM {}",
            measure_mem="G",
            padding=5,
            decorations=[
                BorderDecoration(
                    colour=colors[20],
                    border_width=[0, 0, 2, 0],
                    padding_x=5,
                    padding_y=4,
                )
            ],
        ),
        widget.Sep(
            linewidth=0, padding=6, foreground=colors[10], background=colors[10]
        ),
        # 17
        widget.CPU(
            foreground=colors[20],
            background=colors[10],
            mouse_callbacks={
                "Button1": lambda: qtile.cmd_spawn(terminal1 + " -e gtop")
            },
            fmt="{}",
            # core = 'all',
            padding=5,
            decorations=[
                BorderDecoration(
                    colour=colors[20],
                    border_width=[0, 0, 2, 0],
                    padding_x=5,
                    padding_y=4,
                )
            ],
        ),
        widget.Sep(
            linewidth=0, padding=16, foreground=colors[10], background=colors[10]
        ),
        # 19
        widget.CheckUpdates(
            update_interval=1800,
            distro="Arch_yay",
            display_format="  {updates}",
            no_update_string="",  # 󰚰  󱄋 󰑓
            colour_no_updates=colors[20],
            colour_have_updates=colors[19],
            foreground=colors[19],
            mouse_callbacks={"Button1": lambda: qtile.cmd_spawn("xterm -e yay -Syu")},
            padding=5,
            background=colors[10],
            fontsize=14,
        ),
        widget.Sep(
            linewidth=0, padding=6, foreground=colors[10], background=colors[10]
        ),
        # 21
        widget.GenPollText(
            foreground=colors[20],
            background=colors[10],
            name="checkmails",
            func=lambda: subprocess.check_output(
                HOME + "/.local/scripts/checkmails.sh",
            ).decode("utf-8"),
            # 1800 sec equals to 30 min
            update_interval=60,
            max_chars=3,
            fmt=" {}",
            # foreground=colors['b'],
            mouse_callbacks={
                "Button1": lazy.widget["checkmails"].eval("self.update(self.poll())"),
            },
            decorations=[
                BorderDecoration(
                    colour=colors[20],
                    border_width=[0, 0, 0, 0],
                    padding_x=5,
                    padding_y=4,
                )
            ],
        ),
        widget.Sep(
            linewidth=0, padding=6, foreground=colors[10], background=colors[10]
        ),
        # 23
        widget.Volume(
            foreground=colors[20],
            background=colors[10],
            fmt="VOL {}",
            mouse_callbacks={"Button2": lambda: qtile.cmd_spawn("pavucontrol")},
            padding=5,
            decorations=[
                BorderDecoration(
                    colour=colors[14],
                    border_width=[0, 0, 0, 0],
                    padding_x=5,
                    padding_y=4,
                )
            ],
        ),
        widget.Clock(
            foreground=colors[18],
            background=colors[10],
            format="%A, %d. %B - %H:%M ",
            decorations=[
                BorderDecoration(
                    colour=colors[15],
                    border_width=[0, 0, 0, 0],
                    padding_x=5,
                    padding_y=4,
                )
            ],
        ),
        widget.Sep(
            linewidth=0, padding=6, foreground=colors[10], background=colors[10]
        ),
    ]
    return widgets_list


def init_widgets_screenleft():
    widgets_screenleft = init_widgets_list()
    del widgets_screenleft[11:18]
    return widgets_screenleft  # Monitor 1 will display all widgets in widgets_list


def init_widgets_screenright():
    widgets_screenright = init_widgets_list()
    # Slicing removes unwanted widgets (systray) on Monitors 2
    # del widgets_screenright[9:20]
    del widgets_screenright[9]
    return widgets_screenright


def init_screens():
    return [
        Screen(bottom=bar.Bar(widgets=init_widgets_screenleft(), opacity=1.0, size=30)),
        Screen(
            bottom=bar.Bar(widgets=init_widgets_screenright(), opacity=1.0, size=30)
        ),
    ]


if __name__ in ["config", "__main__"]:
    screens = init_screens()
    widgets_list = init_widgets_list()
    widgets_screenleft = init_widgets_screenleft()
    widgets_screenright = init_widgets_screenright()

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None


# Programms to start on log in
@hook.subscribe.startup
def autostart_once():
    home = os.path.expanduser("~/.config/qtile/autostart.sh")
    subprocess.Popen([home])  # call or Popen?


# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"

# Fixes QT apps
# os.environ["QT_QPA_PLATFORMTHEME"] = "qt5ct"
