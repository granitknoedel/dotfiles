#! /bin/sh
xrandr --output DP1 --primary --mode 2560x1440 --pos 0x0 --rotate normal --output DP2 --mode 2560x1440 --pos 2560x0 --rotate normal --output DP3 --off --output HDMI1 --off --output HDMI2 --off --output HDMI3 --off --output VIRTUAL1 --off

# If the process doesn't exists, start one in background
run() {
	if ! pgrep "$1" ; then
		"$@" &
	fi
}

# Just as the above, but if the process exists, restart it
restart() {
	if ! pgrep "$1" ; then
		"$@" &
	else
		process-restart "$@"
	fi
}

# Sound
# #pactl set-sink-mute 0 toggle
run /usr/bin/pipewire
run /usr/bin/pipewire-pulse
run /usr/bin/pipewire/wireplumber

# Graphical authentication agent
#run /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
run xset b off 						    # Disable beep
# run sxhkd							    # Simple X HKey daemon
run nitrogen --restore 				    # Wallpaper
# run feh --bg-scale ~/.config/wallpapers/wallpaper.jpg
run picom --config "$HOME"/.config/picom/picom.conf 
# run tmux new-session -d -s common	    # Tmux server
run dunst							    # Notification daemon
# run compton						    # Compositor
restart volumeicon 						# Volume icon
run nm-applet 						    # NetworkManager icon
run flameshot                           # Screenshots
run greenclip daemon                    # Clipboard for rofi
run solaar                              # Logitech Devices

# Some process you may want to start with Qtile
run setxkbmap -option ctrl:nocaps       # Disable Capslock
# run cbatticon						    # Battery icon and command
# run xfce4-power-manager 			    # Power management
# restart xfce4-clipman					# Clipboard management
# run mpd --no-daemon					# Music player daemon
