
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias diff='diff --color=auto'

alias ..='cd ..'
alias ...='cd ../../../'
alias ....='cd ../../../../'
alias la='ls -lhAF'
alias ll='ls -lhF'
alias rm='rm -i'
alias rmr='rm -r'
# alias rf='rm -f'
alias mv='mv -iv'
alias cp='cp -riv'            # confirm before overwriting something
alias mkd='mkdir -pv'
alias df='df -h'              # human-readable sizes
alias free='free -m'          # show sizes in MB
alias np='nano -w PKGBUILD'
alias more=less
alias wget='wget -c'
alias pacman='sudo pacman'
alias vi='vim'
alias c='clear'

#sync
alias merge='xrdb -merge ~/XTerm'
alias s='source ~/.bashrc'


#file access
alias zconf='vim ~/.config/zsh/.zshrc'
alias bconf='vim ~/.config/bash/.bashrc'
alias geheim='vim ~/.geheim/vorbeiwort'
alias home='cd ~'

alias qtest='python /home/ninah/.config/qtile/config.py'

