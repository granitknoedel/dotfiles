# Created by Zap installer
[ -f "$HOME/.local/share/zap/zap.zsh" ] && source "$HOME/.local/share/zap/zap.zsh"
plug "zsh-users/zsh-autosuggestions"
plug "zap-zsh/supercharge"
#plug "zap-zsh/zap-prompt"
plug "zsh-users/zsh-syntax-highlighting"

# Example install of a plugin pinned to specifc commit or branch, just pass the git reference
#plug "zsh-users/zsh-syntax-highlighting" "122dc46"

# Example install of a theme
#plug "zap-zsh/zap-prompt"

# Example install of a zsh completion
#plug "esc/conda-zsh-completion"


# Example sourcing of local files
plug "$HOME/.config/zsh/aliases.zsh"
plug "$HOME/.config/zsh/exports.zsh"

plug "$HOME/.config/zsh/themes/agnoster.zsh-theme"

PS1="%n@%m %1~ %#"

