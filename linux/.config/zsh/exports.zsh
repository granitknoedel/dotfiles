
export PATH="$HOME/.local/scripts:$HOME/.local/bin:$HOME/scripts:$PATH"

export EDITOR=nvim
export TERMINAL=alacritty
export TERM=alacritty
export BROWSER="brave"
export OPENER="xdg-open"
export C__LAUNCHER="rofi"

export CMSELECTIONS="clipboard"
export CM_DEBUG=1
export CM_OUTPUT_CLIP=0
export CM_MAX_CLIPS=15
export CM_LAUNCHER="rofi"
