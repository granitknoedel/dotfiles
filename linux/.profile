#!/bin/env sh
# ~/.profile

[[ -f ~/.bashrc ]] && . ~/.bashrc

export PATH="$HOME/.local/scripts:$HOME/.local/bin:$HOME/scripts:$PATH"

# zsh config dir
export ZDOTDIR=$HOME/.config/zsh

# Default programs:
export EDITOR="nvim"
export TERMINAL="alacritty"
export TERMINAL_PROG="alacritty"
export TERM=alacritty
export BROWSER="brave"
export OPENER="xdg-open"
export C__LAUNCHER="rofi"

# ~/ Clean-up:
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export XINITRC="$HOME/.xinitrc"

export CMSELECTIONS="clipboard"
export CM_DEBUG=1
export CM_OUTPUT_CLIP=0
export CM_MAX_CLIPS=15
export CM_LAUNCHER="rofi"

export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc-2.0"
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-3.0/gtkrc-3.0"
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-4.0/gtkrc-4.0"
export WGETRC="$XDG_CONFIG_HOME/wget/wgetrc"
export INPUTRC="$HOME/.inputrc"
export HISTFILE="$HOME/.bash_history"

export CARGO_HOME="$XDG_DATA_HOME/cargo"
export GOPATH="$XDG_DATA_HOME/go"
export GOMODCACHE="$XDG_CACHE_HOME/go/mod"
export ANSIBLE_CONFIG="$XDG_CONFIG_HOME/ansible/ansible.cfg"

. "$HOME/.cargo/env"
