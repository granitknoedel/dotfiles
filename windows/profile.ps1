# Powershell prompt mit Zeitanzeite und simplem Git-Branch Info
# Download/Installieren einer Nerd-Font: https://github.com/ryanoasis/nerd-fonts/releases/download/v3.2.1/RobotoMono.zip
# Im Windows Terminal die Font einstellen

# Install-Module -Name Terminal-Icons -Repository PSGallery
# Install-Module -Name z -Repository PSGallery
Import-Module -Name Terminal-Icons
Import-Module -Name z

function Get-LatestCommit {
    # "git log -n 1" Info uber letzten commit
    $DidMatch = (git log -n 1) -join ' ' -match '(?s)commit (?<commithash>[\w0-9]+)' 
    if ($DidMatch) {
         $Matches.commithash.SubString(0, 6)
    }
}

function prompt {
    $CurrentPosition = $Host.UI.RawUI.CursorPosition
    # Size of current Buffer
    $Buffer = $Host.UI.RawUI.Buffersize
    $WorkingPath = (Get-Location).ToString()
    $WorkingDirectory = Split-Path -Path $(Get-Location) -Leaf
    $FormattedTime = Get-Date -Format "[ HH:mm] ddd, dd.MM.yy"
    $Content = $FormattedTime.ToString()

    # Koords neu berechnen: Top Right - GetDate-Laenge (plus -2 fuer Uhr-Glyphe)
    $Host.UI.RawUI.CursorPosition = [System.Management.Automation.Host.Coordinates]::new($Buffer.Width - $Content.Length -2, $CurrentPosition.Y)
    # Escape-Sequenz "([char]27)" Farb-Code "[38;2;0;200;255m"
    # https://en.wikipedia.org/wiki/ANSI_escape_code
    Write-Host -NoNewline -Object "$([char]27)[38;2;0;200;255m $Content"
    $Host.UI.RawUI.CursorPosition = $CurrentPosition

    if ((git log -n 1 *>&1) -join ' ' -notmatch 'fatal') {
        $currentBranch = (git branch) -replace '\* ', ''
        #Branch  LatestCommit
        #"$([char]27)[38;2;233;78;50m󰊢$([char]27)[39;49m" + ' {1}  {0} ' -f (Get-LatestCommit), ((git branch) -replace '\* ', '')
        # Current directory on Branch
        $formattedPath = " $WorkingDirectory"
        $formattedBranch = "$([char]27)[38;2;233;78;50m $currentBranch$([char]27)[39;49m`n"
        "$formattedPath on $formattedBranch >"
    }
    else {
        ' ' + $WorkingPath + "`n" + '>'
    }
}